FROM openjdk:11-jdk-slim

# Build time variables 
ARG NODE_VERSION=v10.16.0
ARG MAVEN_VERSION=3.6.3

ENV PYTHON /usr/bin/python2.7
ENV M2_HOME=/opt/maven/apache-maven-${MAVEN_VERSION}

# Download required env tools
RUN apt-get update && \
    apt-get install --yes --no-install-recommends curl git wget gnupg2 zip unzip build-essential python2.7 && \

    # Change security level as the SAP npm repo doesnt support buster new security upgrade
    # the default configuration for OpenSSL in Buster explicitly requires using more secure ciphers and protocols,
    # and the server running at http://npm.sap.com/ is running software configured to only provide insecure, older ciphers.
    # This causes SSL connections using OpenSSL from a Buster based installation to fail
    # Should be remove once SAP npm repo will patch the security level
    # see - https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=912759
    sed -i -E 's/(CipherString\s*=\s*DEFAULT@SECLEVEL=)2/\11/' /etc/ssl/openssl.cnf && \

    # install node
    NODE_HOME=/opt/nodejs; mkdir -p ${NODE_HOME} && \
    curl --fail --silent --output - "http://nodejs.org/dist/${NODE_VERSION}/node-${NODE_VERSION}-linux-x64.tar.gz" \
     | tar -xzv -f - -C "${NODE_HOME}" && \
    ln -s "${NODE_HOME}/node-${NODE_VERSION}-linux-x64/bin/node" /usr/local/bin/node && \
    ln -s "${NODE_HOME}/node-${NODE_VERSION}-linux-x64/bin/npm" /usr/local/bin/npm && \
    ln -s "${NODE_HOME}/node-${NODE_VERSION}-linux-x64/bin/npx" /usr/local/bin/ && \
    echo "[INFO] npm -version" && \
	npm -version && \
	echo "[INFO] node --version" && \
	node --version && \
	echo "[INFO] npm install -g grunt-cli" && \
	npm install --prefix /usr/local/ -g grunt-cli && \
    npm install --prefix /usr/local/ -g bower && \
    npm install --prefix /usr/local/ -g gulp && \


    # update maven home
    M2_BASE="$(dirname ${M2_HOME})" && \
    mkdir -p "${M2_BASE}" && \
    echo "[INFO] installing maven." && \
	curl --fail --silent --output - "https://apache.osuosl.org/maven/maven-3/${MAVEN_VERSION}/binaries/apache-maven-${MAVEN_VERSION}-bin.tar.gz" \
    | tar -xzvf - -C "${M2_BASE}" && \
     ln -s "${M2_HOME}/bin/mvn" /usr/local/bin/mvn && \
     chmod --recursive a+w "${M2_HOME}"/conf/* && \
	echo "[INFO] INSTALL COMPLETED"

ENV PATH=$PATH:./node_modules/.bin

WORKDIR /project
